<?php

namespace Drush\Commands\helper;

use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Consolidation\SiteProcess\Util\Escape;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\Exceptions\UserAbortException;
use Drush\Sql\SqlBase;
use Drush\Utils\FsUtils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Finder\Finder;

class SqlCommands extends DrushCommands implements SiteAliasManagerAwareInterface {
  use SiteAliasManagerAwareTrait;

  /**
   * Imports an exported database dump to the database.
   *
   * @param string $file Path to a file containing the SQL to be imported. Gzip files are accepted.
   * @command sql:import
   * @aliases sql-import
   * @option file-delete Delete the file after importing it.
   * @option extra Add custom options to the connect string (e.g. --extra=--skip-column-names)
   * @option drop If provided will drop the database prior to import. Use with caution.
   * @validate-file-exists file
   * @optionset_sql
   * @bootstrap max configuration
   * @usage `drush sql-import example.sql
   *   Bash: Import SQL statements from a file into the current database.
   */
  public function import($file, $options = ['drop' => FALSE, 'file-delete' => FALSE, 'extra' => self::REQ]) {
    $selfRecord = $this->siteAliasManager()->getSelf();

    $sql = SqlBase::create($options);
    $database = $sql->getDbSpec()['database'];

    if (!empty($options['drop'])) {
      if (!$this->io()->confirm(dt('Do you really want to drop all tables in the database !db and import the file !file?', ['!db' => $database, '!file' => basename($file)]))) {
        throw new UserAbortException();
      }
      $drop_options = Drush::redispatchOptions();
      unset($drop_options['drop']);
      unset($drop_options['file-delete']);
      $this->logger()->notice(dt('Dropping database !db.', ['!db' => $database]));
      $process = $this->processManager()->drush($selfRecord, 'sql:drop', [], $drop_options);
      //$process->mustRun($process->showRealtime());
      $process->mustRun();
      $this->output()->writeln($process->getOutput());
    }
    else {
      if (!$this->io()->confirm(dt('Do you really want to import the file !file into the database !db?', ['!db' => $database, '!file' => basename($file)]))) {
        throw new UserAbortException();
      }
    }

    $this->logger()->notice(dt('Importing !file into database !db.', ['!file' => FsUtils::realpath($file), '!db' => $database]));
    $query_options = Drush::redispatchOptions();
    unset($query_options['drop']);
    $process = $this->processManager()->drush($selfRecord, 'sql:query', [], $query_options + ['file' => $file]);
    $process->mustRun();
    $this->output()->writeln($process->getOutput());

    // Re-gzip the file if it was decompressed by sql:query.
    if (!$options['file-delete'] && substr($file, '-3') === '.gz') {
      $file_without_gz = substr($file, 0, -3);
      if (is_file($file_without_gz)) {
        $this->logger()->info(dt('Restoring gzipped file.'));
        $cmd = ['gzip', '-f', Escape::shellArg($file_without_gz)];
        $process = $this->processManager()->shell($cmd);
        $process->disableOutput();
        $process->mustRun();
      }
    }

    $this->logger()->success(dt('Imported !file into database !db.', ['!db' => $database, '!file' => basename($file)]));
  }

  /**
   * Allows the user to select a SQL file to import.
   *
   * @hook interact sql:import
   */
  public function interactSqlImportFile(InputInterface $input) {
    $file = $input->getArgument('file');

    $directory = $this->getConfig()->tmp();
    if (!empty($file)) {
      if (is_file($file)) {
        return;
      }
      elseif (is_dir($file)) {
        $directory = $file;
      }
    }
    else {
      $sql = SqlBase::create($input->getOptions());
      $database = $sql->getDbSpec()['database'];
      $backup_directory = dirname(FsUtils::getBackupDir($database));
      if (is_dir($backup_directory)) {
        $directory = $backup_directory;
      }
    }

    $available_files = $this->getSqlFileOptions($directory);
    if (empty($available_files)) {
      throw new \RuntimeException(dt('No SQL files found in !dir.', ['!dir' => FsUtils::realpath($directory)]));
    }

    $selected = $this->io()->choice(dt('Choose a SQL file to import (showing most recent files first)'), $available_files);
    $input->setArgument('file', $available_files[$selected]);
  }

  protected function getSqlFileOptions($directory) {
    $this->logger()->info(dt('Scanning for *.sql and *.sql.gz files in !dir', ['!dir' => FsUtils::realpath($directory)]));
    $finder = Finder::create()
      ->files()
      ->followLinks()
      ->name('*.sql')
      ->name('*.sql.gz')
      ->sortByModifiedTime()
      ->in($directory);
    $this->logger()->info(dt('Found !count files.', ['!count' => count($finder)]));

    $files = [];
    foreach ($finder as $file) {
      /** @var \Symfony\Component\Finder\SplFileInfo $file */
      $files[] = $file->getRealPath();
    }

    return array_reverse($files);
  }

}
